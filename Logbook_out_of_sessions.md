# Out of sessions Logbook
## Project 4 Year Polytech Grenoble
> Authors : 
BRUN Samuel,
KEMGNE NASAH Darryl Jordan,
LITHAUD Alexandre,
TYNDAL Luca

## Description
The purpose of this is simple, whenever someone does work out of our normal working sessions, he should write down what he's done in this Logbook, and what he intends to do afterwards. This will help others that weren't part of your work session to know what they can do to help with the progression of the Project

## Preliminary actions
I am going to write firstly what I understood what was done by Samuel, but now I have no idea in what direction I should go. I invite anyone who has corrections or modifications with respects to any of the recorded data here to not hesitate in correcting it, especially if you're the one who did the work but not the record.

## Sessions of 13/02/2023 - 17/02/2023
### Code 
Basically, the lexer is done. 
- To be more detailed, tokens where listed in a much more exhaustive way so that many situations that could arise from the parsing can be taken into account. 
- The code was simplified, with respects to it's lecture,  by implementing a lot of auxiliary functions that all help in the execution of the main method.
- A verbose mode was added to the lexer, permitting the compiler to have an option where it print's every token that was parsed during the lecture of the file it's parsing
- A dummy test was created to ensure that the implementation works. With respects to this, the auto-testing file was also upgraded to take this into account.
- The testing script was updated in order to automatically take into account the right number of unit test.
- The testing script was updated to add arguments to the script, -v, -f, -n and -h.

### Doc
Not much was added to the documentation, except for the additional tokens in the Lexer and the reduced Grammar files. In addition to this, this file was also made so that the progress of each and everyone, out of our group sessions can be recorded detailly  manner so that every other person can have a clear idea of what others have done (See description).

