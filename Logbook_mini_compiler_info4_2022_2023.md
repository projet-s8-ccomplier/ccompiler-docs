# Logbook
## Project 4 Year Polytech Grenoble
> Authors : 
BRUN Samuel,
KEMGNE NASAH Darryl Jordan,
LITHAUD Alexandre,
TYNDAL Luca


- [Logbook](#logbook)
  - [Project 4 Year Polytech Grenoble](#project-4-year-polytech-grenoble)
  - [Description](#description)
  - [Session 1:](#session-1)
  - [Session 2:](#session-2)
  - [Session 3:](#session-3)
  - [Session 4:](#session-4)
      - [TO DO](#to-do)
  - [Session 5:](#session-5)
  - [Session 6:](#session-6)
  - [Session 7:](#session-7)
  - [Session 8:](#session-8)
  - [Session 9:](#session-9)
  - [Session 10:](#session-10)
  - [Session 11:](#session-11)


---
## Description 
- At first our mission is to develop a mini C to ASM compiler for the mini CPU named MCPU. The objective is to show the 3A that from the simple tools they see in ALM class, we can play games and do interesting things (with enough memory).
- In a second time (if this one is enough), we will be interested in the second part of the chain, that is to say to build (in Lustre, Digital) this said processor. For that we will use for example the FPGA technology.
- So if the project goes smoothly we will be able to master the whole chain (Circuit, MCPU, compiler, programs).

----

## Session 1: 

* Understanding of the subject, and clarification of the instructions with the teacher
* Reading and appropriation of the available sources
  * The MCPU : 
    * Explanations: https://jeelabs.org/2017/11/tfoc---a-minimal-computer/
    * Git repository: https://github.com/cpldcpu/MCPU
  * The example compilers : 
    * otcc: https://bellard.org/otcc/
    * smallerC: https://github.com/alexfru/SmallerC
    * small-C: https://en.wikipedia.org/wiki/Small-C
    * M2-Planet : https://github.com/oriansj/M2-Planet
  * The code of a former group having already worked on the project:
    * https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/19
* Contact from the former project development team to get a briefing 
* Indantification of potential problems/key points: 
  * self-compilation
  * C restriction (no pointers)
  * memory limits
  * minimal instructions
* Creation of our GitLab repository

----

## Session 2: 

* Redaction of a documentation/summary of the different resources available
* Study of Python scripts for emulation
* Understanding the architecture of the MCPU
* Study of potential problems/needs that may be encountered: 
  * Definition of a C sub-syntax
  * self-compilation
  * Memory limit (potential extension to n bits)
  * FPGA technology 
* Tests/handling of emulation files

----

## Session 3: 

* Studies of available C subsets
* Designing/Writing the rules of our reduced C
* Design/Write a version 1 grammar for the Compilers
* Study of the available code
* Research on regular expressions

----

## Session 4:

* Beginning of the lexer. 
* Making basic functions to read through files and display of characters it contains
* Creating the Makefile
* Definition of the first tokens
* Writing code for testing of functions (some that must compile and others that don't)

#### TO DO
* Complete de lexer (As soon as possible, before next session if possible)
* Start the Syntaxic analyzer by the next session

----

## Session 5:

* correction and finalisation of the lexer
* addition of the test documntation
* addition of the lexer documentation
* start of the parser
* addition of some unitary test

----

## Session 6:

* presentation preparation
* continuation of the parser
* symbol table
* test of code generation

----

## Session 7:

* Theoritical planification of the digital representation
* Finished the symbol table for variables and defines and the functions to manipulate them
* Finished and test the parsing of defines
* Made progress with expression parsing and reviewed the grammar of the expression to have a better parser
* Did some code generation (the first of its kind in the project)

----

## Session 8:

* Advancement of the Digital representation of the processor (Commited in a new branch)
* Continuing the parser
* We are searching for a way to deal with the parsing of expression and function (harder than we expected)
* Created an immediate table in order to deal with the parsing of expression 
* Studying deeply the ASM assembly

----

## Session 9:
* Comprehension and first simulation on digital. Part of it works, but further reviews need to be made
* Coding and testing the code generator using a simple int statement, and testing it
* Continuing the code of the parser, done with quite a big part, but need to really resee the grammar and parser for the expressions
* Resolved some issues with the code generation and succeeded in generating our first line of code

----

## Session 10:

* Parsing of the soustraction and variable
* Advencement in the digital representation of the MCPU
* We now need to be parsing all the arithmetic formula before commencing the parsing of the condition and loops
* Corrected minor bugs in the parsing
* Changed a few assembly generating code to increse performance

----

## Session 11:

* Finalization of all the arithmetic formula parsing. (Addition, Sustraction, Multiplication and Division done)
* Continuing the digital representation (problem with the link to the RAM)
* Finalization of the expression parsing
* Started to implement the parsing of if conditions
* Added priority to arithmetic expression
* Comment part of the code that was missing explainations

## Session 12

* Generating the code for the if statement. All comparison conditions (<=, <, >, >=, ==, != ) working 
* For the if statement, the else part of the statement is taken into account, and the not (!) logical statement works.
* The logical comparators in the if statement (&&, ||) are being worked on
* Started the report for the project
* Redid the documentation for it to better fit the instructions that were given