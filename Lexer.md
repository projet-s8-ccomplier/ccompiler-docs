main function : NextToken


## Token types
|           Name           | Int Id |     Equivalent     |
|:------------------------:|:------:|:------------------:|
|       IDENT_TOKEN        |   0    | The name by string |
|                          |        |                    |
|         IF_TOKEN         |   10   |         if         |
|        ELSE_TOKEN        |   11   |        else        |
|         DO_TOKEN         |   12   |         do         |
|       WHILE_TOKEN        |   13   |       while        |
|        INT_TOKEN         |   14   |        int         |
|        CHAR_TOKEN        |   15   |        char        |
|       RETURN_TOKEN       |   16   |       return       |
|       DEFINE_TOKEN       |   17   |      #define       |
|                          |        |                    |
|      OPEN_PAR_TOKEN      |   30   |         (          |
|     CLOSE_PAR_TOKEN      |   31   |         )          |
|     OPEN_BRACK_TOKEN     |   32   |         {          |
|    CLOSE_BRACK_TOKEN     |   33   |         }          |
| OPEN_SQUARE_BRACK_TOKEN  |   34   |         [          |
| CLOSE_SQUARE_BRACK_TOKEN |   35   |         ]          |
|     SEMICOLON_TOKEN      |   36   |         ;          |
|        DOT_TOKEN         |   37   |         .          |
|                          |        |                    |
|        ADD_TOKEN         |   50   |         +          |
|        SUB_TOKEN         |   51   |         -          |
|        DIV_TOKEN         |   52   |         /          |
|        MOD_TOKEN         |   53   |         %          |
|        STAR_TOKEN        |   54   |         *          |
|        ADR_TOKEN         |   55   |         &          |
|                          |        |                    |
|        NOT_TOKEN         |   60   |         !          |
|        AND_TOKEN         |   61   |         &&         |
|         OR_TOKEN         |   62   |        \|\|        |
|                          |        |                    |
|         EQ_TOKEN         |   70   |         ==         |
|        NEQ_TOKEN         |   71   |         !=         |
|        LESS_TOKEN        |   72   |         <          |
|      LESS_EQ_TOKEN       |   73   |         <=         |
|        MORE_TOKEN        |   74   |         >          |
|      MORE_EQ_TOKEN       |   75   |         >=         |
|                          |        |                    |
|       ASSIGN_TOKEN       |   80   |         =          |
|                          |        |                    |
|    INT_LITERAL_TOKEN     |   90   |      entiers       |
|   STRING_LITERAL_TOKEN   |   91   |       String       |
|    CHAR_LITERAL_TOKEN    |   92   |     characters     |
|                          |        |                    |
|       ERROR_TOKEN        | 32765  |        ERR         |
|        EOF_TOKEN         | 32766  |        EOF         |
