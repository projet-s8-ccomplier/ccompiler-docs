# MCPU from Tim Böscke 

link : https://jeelabs.org/2017/11/tfoc---a-minimal-computer/`

Purpose : create an assembler for MCPU and emulate (in python)
MCPU is a minimal cpu aimed to fit into a 32 Macrocell CPLD - one of the smallest available programmable logic device

Le processeur parcours le programme en mémoire et execute les opérations qu'il lit (opéraiton binaires / hexa prédéfinies). Le processeur a 2^6 octets de mémoire.

Trace d'un programme : 
```C
pc,ir,acc,cf:   0   0x3e    0   0
pc,ir,acc,cf:   1   0x45    0   0
pc,ir,acc,cf:   2   0x7f    250 0
pc,ir,acc,cf:   3   0xc2    251 0
pc,ir,acc,cf:   2   0x7f    251 0
pc,ir,acc,cf:   3   0xc2    252 0
pc,ir,acc,cf:   2   0x7f    252 0
pc,ir,acc,cf:   3   0xc2    253 0
pc,ir,acc,cf:   2   0x7f    253 0
pc,ir,acc,cf:   3   0xc2    254 0
pc,ir,acc,cf:   2   0x7f    254 0
pc,ir,acc,cf:   3   0xc2    255 0
pc,ir,acc,cf:   2   0x7f    255 0
pc,ir,acc,cf:   3   0xc2    0   1
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
```
Ce programme est equivalent a celui-ci, ecrit en assembleur MCPU: 
```assembly
        org 0
        lda count
cloop   add one
        jcc cloop

done    jcc done

count   dcb 250
```
Ce code :
- charge dans l'accumulateur un compteur qui compte de 0 à 255, et déborde a 0, avant de s'arreter.
>load a count in the accumulator, add one, and loop until the carry flag is set. Then we enter an infinite loop.

C'est l'assembleur MCPU qui sera compilé en hexa pour l'émulateur présenté ci-dessous.

Script simulateur :
[Script](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/blob/main/ScriptPython/asm.py)

Voilà le code d'un "two-pass" assembleur en script python. 
"Two-pass" car il éffectue deux parcours sur le code source afin de savoir si il doit exécuter des lignes qui contiennent des variables qui seront définis plutard (labels). Il fait donc une premiere passe pour localisé les label et enregistrer l'adresse de PC associé. Lors de la seconde passe, il traduit les différentes instructions.

L'objectif de ce premier script est de convertir une entrée de la forme assembleur MCPU vers un code Hexa.

Ce code hexa peut ensuite etre executé grace au code python ci-dessous : 
[Script](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/blob/main/ScriptPython/emu.py)

On peut donc executer un programme assembleur MCPU en faisant la suite de commmande suivante : 

python asm.py < <PROGRAM.asm> | python emu.py 200000

le 200 000 correspond au nombre de cycles procésseur pour la simulation.

> exemple commande : 
> python asm.py < primeNumber.obj | python emu.py 200000