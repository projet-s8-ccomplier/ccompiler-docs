# Documentation MCPU - GitHub

- [Documentation MCPU - GitHub](#documentation-mcpu---github)
  - [Courte description de MCPU](#courte-description-de-mcpu)
  - [Architecture de la carte](#architecture-de-la-carte)
  - [Les instructions](#les-instructions)
  - [Les Macros](#les-macros)
  - [Exemple de code ASM commenté](#exemple-de-code-asm-commenté)
  - [Partie Contrôle](#partie-contrôle)
  - [Problématique](#problématique)
  - [Proposition de solution :](#proposition-de-solution-)

## Courte description de MCPU

Le MCPU est un processeur qui avais pour objectif de fonctionner sur une 32 Macrocell CPLD(lien) l'un des plus petit outils logique programmable.

Il n'a pas pour objectif de fonctionner dans de vrai condition, il en serai même incapable.

Cette carte est en 8 bits il en utilise 2 pour les instruction il ne possède donc que 2^6 donc 64 octets de RAM.

## Architecture de la carte

Le CPU suit cette description :
```basic
entity CPU8BIT2 is
port (
    data: inout std_logic_vector(7 downto 0);
    adress: out std_logic_vector(5 downto 0);
    oe: out std_logic;
    we: out std_logic;
    rst: in std_logic;
    clk: in std_logic);
end;
```

- Le CPU possède 8 bits pour les données qui sont à la fois en entré et en sortie.
- Le CPU possède 6 bits pour les adresse qui sont uniquement en sortie.
- oe et we sont des flag uniquement en sortie.
- rst et clk sont des flag uniquement en entrée.


## Les instructions

| Mnemonic | Code |    Opcode    |
|:--------:|:----:|:------------:|
|   NOR    |  00  | [CODE]AAAAAA |
|   ADD    |  01  | [CODE]AAAAAA |
|   STA    |  10  | [CODE]AAAAAA |
|   JCC    |  11  | [CODE]DDDDDD |

![L'architecture du MCPU](./../images/Architecture.png)

- On possède en réalité un seul registre matérielle nommé Akku 
    - NOR réalise l'opération NOR entre la donné situé  dans Akku et la donné situé dans l'espace mémoire AAAAAA et la stock dans Akku
    - ADD  réalise l'opération ADD entre la donné situé dans AKKU et la donné situé dans l'espace mémoire AAAAAA et la stock dans Akku et met à jour C(1 si le résultat est positif 0 sinon)
    - STA met à l'address AAAAAA la donné situé dans Akku
    - JCC met à jour le PC si C est à 1 avec l'address DDDDDD et remmet C à 0
- On peut réalisé des instructions plus complexe en faisant des mélanges d'instruction de base


## Les Macros

Le processeur n'ayant que 2 bits pour les instructions, il ne possède que 2^2 donc 4 instruction max (voir ci-dessus).

Il existe donc des macro qui sont des suites de fonction assembleurs qui execute une fonction spécifique.

|   Macro   |             Code Assembler             |
|:---------:|:--------------------------------------:|
|    CLR    |             **NOR** allone             |
| LDA [mem] |      **NOR** allone, **ADD** mem       |
|    NOT    |              **NOR** zero              |
| JMP [dst] |        **JCC** dst, **JCC** dst        |
| JCS [dst] |       **JCC** \*+2, **JCC** dst        |
| SUB [mem] | **NOR** zero, **ADD** mem, **ADD** one |

**allone = 0xFF**
**zero = 0x00**
**one = 0x01**

- CLR permet de clear ou vider l'accumulateur (Akku)
- LDA [mem] permet de load mem dans l'accumulateur (Akku)
- NOT permet d'inverser le contenue de l'accumulateur (Akku)
- JMP [dst] permet de sauter à la destination dans tous les cas
- JCS [dst] permet de sauter si la retenue sortante est à vrai.
- SUB [mem] permet de soutraire la valeur mem à l'accumulateur (Akku)

## Exemple de code ASM commenté

```arm
start :
    NOR allone              ; Akku = 0 => CLR !
    NOR b                   ; NOT !
    ADD one                 ; Akku = − b
    ADD a                   ; Akku = a − b
                            ; Carry set when akku >= 0
    JCC neg
    STA a
    ADD allone
    JCC end                 ;A=0 ? −> end , result in b
    JCC start
neg :
    NOR zero                ; NOT !
    ADD one                 ; Akku = −Akku
    STA b
    JCC start               ; Carry was not altered
end :
    JCC end
```

Ce programme calcul le pgcd de deux nombre a et b grace à la technique de Dijkstra.

## Partie Contrôle

Le CPU utilse une machine à été à 5 états. Voici un shéma représentant :

![La partie controle](./../images/PC.png)

S0 = 000
S1 = 001
S2 = 010
S3 = 011
S5 = 101

## Problématique

Avec 64 octets de RAM nous n'avons pas la place de faire de gros programme C car on manquerai de place de stockage du code.

De plus le CPU ne possède pas de registre ce qui implique que la RAM devra stoker toutes les données.

Donc en 8 bits le processeur est extremement limité et possède de nombreuses contraintes que nous devons résoudre afin de pouvoir faire fonctionner un compilateur C dessus.

## Proposition de solution :

Afin de résoudre ces problèmes nous avons plusieurs solutions :
- Augmenter la taille du CPU de façon fixe en passant de 8 bits à 16 ou 32 bits par exemple. Cela augmenterai considérablement la RAM et permetterai de d'avoir la place de stockage suffisante afin de pourvoir tout réaliser.

**OU**

- Augmenter la taille du CPU de façon dynamique. En utilisant la technologie FPGA afin de pouvoir changer dynamiquement la taille du CPU. Dans ce cas la carte aurai :
    - n bits d'entrée/sortie pour les données
    - n-2 bits de sortie pour les adresse
