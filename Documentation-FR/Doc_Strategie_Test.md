# Documentation de la Stratégie de test

- [Documentation de la Stratégie de test](#documentation-de-la-stratégie-de-test)
  - [Description de la stratégie de test](#description-de-la-stratégie-de-test)
  - [Script de test](#script-de-test)
    - [Fonctionnement du script](#fonctionnement-du-script)
    - [Exemple d'utilisation du script](#exemple-dutilisation-du-script)

## Description de la stratégie de test

Pour notre compilateur nous avons besoin de test complet et efficace qui nous permetterons de tester les différentes étapes de la compilation. 

Notre compilateur passe d'abord par l'étape de lexer (qui va extraire les token). Puis par le parsing des tokens dans le code assembleur MCPU.

Nous avons donc choisie de faire une série de test unitaire (nommé `<numero_de_test>.c`) qui va permettre de tester un spécification spéciale (un boucle par exemple).

Nos tests sont séparé en 2 dossiers distincts : fonctionel et non fonctionnelle. 
- [Fonctionnel](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/test_unitaire/fonctionel) représente du code qui sera fonctionelle dans le langage small-C. **Ces tests devront être "PASSED"**.
- [Non Fonctionnel](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/test_unitaire/non_fonctionel) représente du code qui ne fonctionnerai pas dans le langage small-C. Ces test permettent de détecter des erreurs que les tests fonctionnel ne pourra pas détecter. **Ces test devront être "FAILED"**.

Le resulat de tout nos test seront situé dans le dossier [`result_test`](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/result_test) qui stockera le résultat de chaque prompt de test dans un fichier nommé : `result_<num_de_test>.txt`.

Le début de fichier correspond au tokens détecter par le lexer. Si un erreur est détecter la ligne en question sera retounée.

Le dernière ligne de test sera soit `TEST PASSED` ou `TEST FAILED` en fonction de si une erreur c'est dérouler pendant la compilation.

## Script de test

Afin de pouvoir rapidement et efficacement réaliser tous les tests unitaires nous avons créer un bash nomé [`test_launcher.sh`](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/blob/main/test_launcher.sh). 

### Fonctionnement du script

Ce script permet de lancer automatiquement tous les test et d'afficher les résulats dans le dossier [`result_test`](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/result_test).

Il possède plusier arguments permettant d'activé des différentes fonctions du programme.

- **-h :** Permet d'afficher l'aide de l'application
- **-v :** Permet d'activé le mode verbeux du script. Dans ce mode le scipt affichera à la fin de l'execution le résultat de chaque test. (soit `TEST PASSED` ou `TEST FAILED`).
- **-f \<nombre> :** Permet de lancer seulement le \<nombre> de test fonctionel
- **-n \<nombre> :** Permet delancer seulement le \<nombre> de test non fonctionel

### Exemple d'utilisation du script

- TEST BASIQUE : `bash test_launcher.sh`
- TEST AVEC AFFICHAGE DES RESULTAT : `bash test_launcher.sh -v`
- TEST AVEC 10 TEST FONCTIONEL : `bash test_launcher.sh -f 10`
- TEST AVEC 15 TEST NON FONCTIONEL : `bash test_launcher.sh -n 15`
- TEST AVEC 10 TEST F ET 5 TEST NF : `bash test_launcher.sh -f 10 -n 5`
- AFFICHAGE DE L'HELPER : `bash test_launcher.sh -h`
- AFFICHAGE DE L'HELPER PAR ERREUR : `bash test_launcher.sh -y`

