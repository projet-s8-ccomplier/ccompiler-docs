# MCPU from Tim Böscke 

link : https://jeelabs.org/2017/11/tfoc---a-minimal-computer/`

Purpose : create an assembler for MCPU and emulate (in python)
MCPU is a minimal cpu aimed to fit into a 32 Macrocell CPLD - one of the smallest available programmable logic device

The processor runs the program in memory and executes the operations it reads (predefined binary/hexa operations). The processor has 2^6 bytes of memory.

Trace of a program :
```C
pc,ir,acc,cf:   0   0x3e    0   0
pc,ir,acc,cf:   1   0x45    0   0
pc,ir,acc,cf:   2   0x7f    250 0
pc,ir,acc,cf:   3   0xc2    251 0
pc,ir,acc,cf:   2   0x7f    251 0
pc,ir,acc,cf:   3   0xc2    252 0
pc,ir,acc,cf:   2   0x7f    252 0
pc,ir,acc,cf:   3   0xc2    253 0
pc,ir,acc,cf:   2   0x7f    253 0
pc,ir,acc,cf:   3   0xc2    254 0
pc,ir,acc,cf:   2   0x7f    254 0
pc,ir,acc,cf:   3   0xc2    255 0
pc,ir,acc,cf:   2   0x7f    255 0
pc,ir,acc,cf:   3   0xc2    0   1
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
pc,ir,acc,cf:   4   0xc4    0   0
```
This program is equivalent to this one, written in MCPU assembly:
```assembly
        org 0
        lda count
cloop   add one
        jcc cloop

done    jcc done

count   dcb 250
```
This code :
- loads a counter into the accumulator that counts from 0 to 255, and overflows to 0, before stopping.
>Load a count in the accumulator, add one, and loop until the carry flag is set. Then we enter an infinite loop.

This is the MCPU assembler that will be compiled in hexa for the emulator shown below.

Script simulator :
[Script](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/blob/main/ScriptPython/asm.py)

Here is the code of a "two-pass" assembler in python script. 
"Two-pass" because it performs two runs on the source code to know if it should execute lines that contain variables that will be defined later (labels). It then makes a first pass to locate the labels and record the associated PC address. During the second pass, it translates the different instructions.

The objective of this first script is to convert an input from MCPU assembly form to Hexa code.

This hexadecimal code can then be executed using the python code below:
[Script](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/blob/main/ScriptPython/emu.py)

We can therefore run an MCPU assembly program by doing the following command sequence: 

python asm.py < <PROGRAM.asm> | python emu.py 200000

The 200,000 corresponds to the number of processor cycles for the simulation.

> example command : 
> python asm.py < primeNumber.obj | python emu.py 200000