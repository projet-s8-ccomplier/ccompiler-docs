# Documentation of the Testing Strategie

- [Documentation of the Testing Strategie](#documentation-of-the-testing-strategie)
  - [Description of the Testing Strategie](#description-of-the-testing-strategie)
  - [Test's Script](#tests-script)
    - [Script Working](#script-working)
    - [Use cases of the script](#use-cases-of-the-script)

## Description of the Testing Strategie

For our compiler we need a complete and efficient test that will allow us to test the different steps of the compilation.

Our compiler first goes through the lexer stage (which will extract the tokens). Then by parsing the tokens into the MCPU assembly code.

We have therefore chosen to make a series of unit tests (named `<numero_of_test>.c`) that will allow us to test a special specification (a loop for example).

Our tests are separated into two distinct files: functional and non-functional.
- [Functional](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/test_unitaire/fonctionel) represents code that will be functional in the small-C language. **These tests should be "PASSED "**.
- [Non-Functional](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/test_unitaire/non_fonctionel) represents code that would not work in the small-C language. These tests are used to detect errors that functional tests will not detect. **These tests should be "FAILED "**.

The results of all our tests will be located in the folder [`result_test`](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/result_test) which will store the results of each test prompt in a file named: `result_<test_num>.txt`.

The beginning of the file corresponds to the tokens detected by the lexer. If an error is detected the line in question will be returned.

The last test line will be either `TEST PASSED` or `TEST FAILED` depending on whether an error occurred during compilation.

## Test's Script

In order to be able to quickly and efficiently perform all the unit tests we have created a bash named [`test_launcher.sh`](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/blob/main/test_launcher.sh).

### Script Working

This script automatically runs all tests and displays the results in the [`result_test`] folder (https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/10/code/-/tree/main/result_test).

It has several arguments to activate the different functions of the program.

- **-h :** Display the helper
- **-v :** Verbose Mode. In this mode the script will print the result of every tests possible. (either `TEST PASSED` or `TEST FAILED`).
- **-f \<number> :** Launch only the \<number> of fonctional tests
- **-n \<number> :** Launch only the \<number> of non fonctional tests

### Use cases of the script

- BASIC TEST COMMAND: `bash test_launcher.sh`
- DISPLAY RESULT TEST COMMAND: `bash test_launcher.sh -v`
- 10 FONCTIONAL TEST COMMAND: `bash test_launcher.sh -f 10`
- 15 NON FONCTIONAL TEST COMMAND: `bash test_launcher.sh -n 15`
- 10 FONCTIONAL 5 NON FONCTIONAL TEST COMMAND: `bash test_launcher.sh -f 10 -n 5`
- HELP DISPLAY: `bash test_launcher.sh -h`
- HELP DISPLAY BY WRONG COMMAND : `bash test_launcher.sh -y`

